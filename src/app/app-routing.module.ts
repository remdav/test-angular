import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RouteFront } from 'src/type/route';
import { IndexComponent } from './index/index.component';
import { InfoComponent } from './user/info/info.component';
import { LoginComponent } from './user/login/login.component';

const routes: Routes = [
  { path: RouteFront.INDEX, component: IndexComponent },
  { path: RouteFront.LOGIN, component: LoginComponent },
  { path: RouteFront.INFO_USER, component: InfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

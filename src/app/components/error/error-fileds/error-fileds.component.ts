import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-error-fileds',
  templateUrl: './error-fileds.component.html',
  styleUrls: ['./error-fileds.component.css'],
})
export class ErrorFiledsComponent implements OnInit {
  @Input() form: any;
  @Input() name: any;

  constructor() {}

  ngOnInit(): void {}
}

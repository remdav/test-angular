import { Component, OnInit } from '@angular/core';
import { RouteFront, TitlePage } from 'src/type/route';
import { PageService } from '../services/helpers/global/page.ts.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
})
export class IndexComponent implements OnInit {
  internalUrls = RouteFront;

  constructor(private page: PageService) {}

  ngOnInit(): void {
    this.page.setTitle(TitlePage.INDEX);
  }
}

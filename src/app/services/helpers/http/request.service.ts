import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UriBack } from 'src/type/route';
import { NameLocalStorage } from 'src/type/storage';
import { LocalstorageService } from '../storage/localstorage.service';

type Method = 'GET' | 'POST';

@Injectable({
  providedIn: 'root',
})
export class RequestService {
  constructor(private readonly localStorage: LocalstorageService) {}

  private async request(uri: UriBack, method: Method, body = {}): Promise<any> {
    const storageUser = this.localStorage.getStorage(NameLocalStorage.USER);

    let result;

    let requestData: any = {
      method,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${
          storageUser.accessToken ?? environment.access_app_api
        }`,
      },
      mode: 'cors',
    };

    if (Object.keys(body).length > 0) {
      requestData = {
        ...requestData,
        body: JSON.stringify(body),
      };
    }

    try {
      result = await fetch(`${environment.api_url}/${uri}`, requestData);
    } catch (error) {
      throw new Error();
    }

    return await result.json();
  }

  async get(uri: UriBack): Promise<any> {
    return await this.request(uri, 'GET');
  }

  async post(uri: UriBack, body: { [key: string]: any }): Promise<any> {
    return await this.request(uri, 'POST', body);
  }
}

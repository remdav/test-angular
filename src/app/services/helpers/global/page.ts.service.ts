import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TitlePage } from 'src/type/route';

@Injectable({
  providedIn: 'root',
})
export class PageService {
  constructor(private title: Title) {}

  setTitle(title: TitlePage): void {
    this.title.setTitle(title);
  }
}

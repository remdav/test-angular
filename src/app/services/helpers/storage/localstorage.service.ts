import { Injectable } from '@angular/core';
import { NameLocalStorage } from 'src/type/storage';

@Injectable({
  providedIn: 'root',
})
export class LocalstorageService {
  constructor() {}

  getStorage(name: NameLocalStorage): any {
    const storage = localStorage.getItem(`@${name}`);
    if (storage) {
      return JSON.parse(localStorage.getItem(`@${name}`) || '');
    } else {
      return {};
    }
  }

  setStorage(name: NameLocalStorage, data: string | { [key: string]: any }) {
    localStorage.setItem(`@${name}`, JSON.stringify(data));
  }

  removeStorage(name: NameLocalStorage) {
    localStorage.removeItem(`@${name}`);
  }
}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RouteFront } from 'src/type/route';
import { NameLocalStorage } from 'src/type/storage';
import { LocalstorageService } from '../storage/localstorage.service';

@Injectable({
  providedIn: 'root',
})
export class SecureFrontService {
  constructor(
    private readonly localStorage: LocalstorageService,
    private readonly router: Router
  ) {}

  verifyStorage(): void {
    const storage = this.localStorage.getStorage(NameLocalStorage.USER);

    if (!storage.accessToken) {
      this.router.navigate([`/${RouteFront.LOGIN}`]);
    }
  }
}

import { Injectable } from '@angular/core';
import { FormLogin, ResponseDataUser, ResponseLogin } from 'src/type/app/user';
import { UriBack } from 'src/type/route';
import { RequestService } from '../helpers/http/request.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly request: RequestService) {}

  async login(data: FormLogin): Promise<ResponseLogin> {
    return await this.request.post(UriBack.LOGIN, data);
  }

  async getDataUser(): Promise<ResponseDataUser> {
    return await this.request.get(UriBack.INFO_USER);
  }
}

import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ErrorFiledsComponent } from './components/error/error-fileds/error-fileds.component';
import { IndexComponent } from './index/index.component';
import { InfoComponent } from './user/info/info.component';
import { LoginComponent } from './user/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    LoginComponent,
    ErrorFiledsComponent,
    InfoComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [Title],
  bootstrap: [AppComponent],
})
export class AppModule {}

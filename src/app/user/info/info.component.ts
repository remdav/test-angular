import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/app/user.service';
import { SecureFrontService } from 'src/app/services/helpers/secure/secure-front.service';
import { LocalstorageService } from 'src/app/services/helpers/storage/localstorage.service';
import { ResponseDataUser } from 'src/type/app/user';
import { RouteFront } from 'src/type/route';
import { NameLocalStorage } from 'src/type/storage';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css'],
})
export class InfoComponent implements OnInit {
  myInfo: ResponseDataUser | null = null;

  constructor(
    private readonly userService: UserService,
    private readonly localStorage: LocalstorageService,
    private readonly router: Router,
    private readonly secureRoute: SecureFrontService
  ) {}

  async ngOnInit(): Promise<void> {
    this.secureRoute.verifyStorage();
    this.myInfo = await this.getData();
  }

  private async getData(): Promise<ResponseDataUser> {
    return await this.userService.getDataUser();
  }

  logout(): void {
    this.localStorage.removeStorage(NameLocalStorage.USER);
    this.router.navigate([`/${RouteFront.INDEX}`]);
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/app/user.service';
import { RouteFront, TitlePage } from 'src/type/route';
import { NameLocalStorage } from 'src/type/storage';
import { PageService } from '../../services/helpers/global/page.ts.service';
import { LocalstorageService } from '../../services/helpers/storage/localstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  formLogin: any;

  constructor(
    private page: PageService,
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly localStorage: LocalstorageService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.page.setTitle(TitlePage.LOGIN);
    this.createForm();
  }

  private createForm() {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', Validators.required],
    });
  }

  async onSubmit() {
    const result = await this.userService.login(this.formLogin.value);

    if (result.accessToken) {
      this.localStorage.setStorage(NameLocalStorage.USER, result);
      this.router.navigate([`/${RouteFront.INFO_USER}`]);
      return;
    }
  }
}

export enum RouteFront {
  INDEX = '',
  LOGIN = 'se-connecter',
  INFO_USER = 'information',
}

export enum TitlePage {
  INDEX = 'Test angular',
  LOGIN = 'Se connecter',
}

export enum UriBack {
  LOGIN = 'user/login',
  INFO_USER = 'user',
}

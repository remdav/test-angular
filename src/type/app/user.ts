export interface FormLogin {
  username: string;
  password: string;
}

export interface ResponseLogin {
  accessToken: string;
  refreshToken: string;
}

export interface ResponseDataUser {
  username: string;
  role: string[];
}
